<?php

require_once "animal.php" ;
require_once "ape.php" ;
require_once "frog.php" ;

$animal_1 = new animal("") ;

echo "Name : " . $animal_1->name ."<br>" ;
echo "Legs : " . $animal_1->legs . "<br>" ;
echo "Cold Blooded : " . $animal_1->cold_blooded . "<br>" ;

echo "<br>" ;

$animal_2 = new ape("kera sakti") ;

echo "Name : " . $animal_2->name . "<br>" ;
echo "Legs : " . $animal_2->legs . "<br>" ;
echo "Cold Blooded : " . $animal_2->cold_blooded . "<br>" ;
$animal_2->yell() ;

echo "<br>" ;

$animal_3 = new frog("buduk") ;


echo "Name : " . $animal_3->name . "<br>" ;
echo "Legs : " . $animal_3->legs . "<br>" ;
echo "Cold Blooded : " . $animal_3->cold_blooded . "<br>" ;
$animal_3->jump();
